#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "cpu.h"

unsigned char fontset[80] =
{
	0xF0, 0x90, 0x90, 0x90, 0xF0, 	// 0
	0x20, 0x60, 0x20, 0x20, 0x70,	// 1
	0xF0, 0x10, 0xF0, 0x80, 0xF0, 	// 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0,	// 3
	0x90, 0x90, 0xF0, 0x10, 0x10, 	// 4
	0xF0, 0x80, 0xF0, 0x10, 0xF0,	// 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0, 	// 6
	0xF0, 0x10, 0x20, 0x40, 0x40, 	// 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0,	// 8
	0xF0, 0x90, 0xF0, 0x10, 0xF0,	// 9
	0xF0, 0x90, 0xF0, 0x90, 0x90,	// A
	0xE0, 0x90, 0xE0, 0x90, 0xE0,	// B
	0xF0, 0x80, 0x80, 0x80, 0xF0,	// C
	0xE0, 0x90, 0x90, 0x90, 0xE0,	// D
	0xF0, 0x80, 0xF0, 0x80, 0xF0,	// E
	0xF0, 0x80, 0xF0, 0x80, 0x80	// F
};

void load_file(char game[20]);
int fetch_execute(unsigned short opcode);
int initialize();
int main();

int initialize() {
	pc = 0x200;	// 512, starting address for programs
	i = 0;
	sp = 0;
	delay_timer = 0;
	sound_timer = 0;

	for(int i = 0; i <= MEM_LEN; i++) {
		ram[i] = 0;
	}

	for(int i = 0; i < 80; i++) {
		ram[i] = fontset[i];
	}


	return 0;
}

void load_file(char game[50]) {
	
	FILE *fp;
	char *buffer;
	unsigned short file_size;

	/* open file in read-binary mode */
	if((fp = fopen(game, "rb")) == NULL) {
		perror("could not read file ");
		abort();
	}
	
	/* get the size of the file */
	if(fseek(fp, 0, SEEK_END) != 0) {
		perror("could not scan file ");
		abort();
	} else{
		file_size = ftell(fp);
		rewind(fp);
	}
	
	/* allocate enough memory to store instructions in file */
	if((buffer = (char *)malloc(sizeof(char) * file_size)) == NULL) {
		perror("could not allocate memory ");
		abort();
	}
	
	/* read file into buffer */
	if(fread(buffer, sizeof(char), file_size, fp) == 0) {
		perror("could not read from file ");
		abort();
	}
	
	/* store buffer in memory */
	for(int x = 0; x < file_size; x++) {
		ram[x+512] = (ushort)buffer[x];
	}
	
	fclose(fp);
	free(buffer);
}

int fetch_execute(unsigned short opcode) {
	
	opcode = ram[pc] << 8 | ram[pc+1];

	switch(opcode & 0xF000) {	// view the first nibble of the opcode to determine function
		case 0x0000:
			switch(opcode & 0x000F) {
				case 0x00e0:
				/* clears the screen (64 * 32) */
					for(int x = 0; x <= (DIS_HEIGHT * DIS_WIDTH); x++) {
							display[x] = 0;	// change pixel to black
						}
					pc+=2;	// increments program counter by 2
					break;
				case 0x00ee:
					/* returns from a subroutine */
					sp -= 1;
					pc = stack[sp];
					break;
				default:
					printf("Unknown opcode [0x0000]: 0x%X\n", opcode);
				}
			//break;
			case 0x1000:	// 0x3NNN
				/* jump to address at NNN  */
				pc = opcode & 0x0FFF;
				break;
			case 0x2000:	// 0x2NNN 
				/* call subroutine at NNN */
				stack[sp++]=+2;
				pc = opcode & 0x0FFF;
				break;
			case 0x3000:	// 0x3XNN
				/* if VX is the same as NN then skip next instrucution */
				if(v[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF)) {
					pc+=4;
				}
				else {
					pc+=2;
				}
				break;
			case 0x4000:	// 0x4XNN
				/* skips next instruction if VX is the same as NN */
				if(v[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF)) {
					pc+=2;
				}
				else {
					pc+=2;
				}
				break;
			case 0x5000:	// 0x5XY0
				/* skips the next instruction if VX equals VY */
				if(v[(opcode & 0x0F00) >> 8] == (v[(opcode & 0x00F0) >> 4])) {
					pc+=4;
				}
				else {
					pc+=2;
				}
				break;
			case 0x6000:	// 0x6XNN
				/* set VX to NN */
				v[(opcode & 0x0F00) >> 8] = opcode & 0x00FF;
				pc+=2;
				break;
			case 0x7000:	// 0x7XNN
				/* set VX to VX OR VY */
				v[(opcode & 0x0F00) >> 8] += opcode & 0x00FF;
				pc+=2;
				break;
			case 0x8000:
				switch(opcode & 0xFFF0) {
					case 0x0000:	// 0x8XY0
						/* set sets VX to VY */
						v[opcode & 0x0F00] = v[opcode & 0x00F0];
						pc+=2;
						break;
					case 0x0001:	// 0x8XY1
						/* sets VX to VX OR VY */
						v[(opcode & 0x0F00) >> 8] = v[(opcode & 0x0F00) >> 8] | v[(opcode & 0x00F0) >> 4];
						pc+=2;
						break;
					case 0x0002:	// 0x8XY2
						/* sets VX to VX AND VY */
						v[(opcode & 0x0F00) >> 8] = v[(opcode & 0x0F00) >> 8] & v[(opcode & 0x00F0) >> 4];
						pc+=2;
						break;
					case 0x0003:	// 0x8XY3
						/* sets VX to VX XOR VY */
						v[(opcode & 0x0F00) >> 8] = v[(opcode & 0x0F00) >> 8] ^ v[(opcode & 0x00F0) >> 4];
						pc+=2;
						break;
					case 0x0004:	// 0x8XY4
						/* adds VY to VX. VF is set to 0 if there is a borrow required, or 1 if there isn't */
						v[(opcode & 0x0F00) >> 8] += v[(opcode & 0x00F0) >> 4];
						if(v[(opcode & 0x00F0) >> 4] > (0xFF - v[(opcode & 0x0F00) >> 8])) {
							v[0xF] = 1;
						}
						else {
							v[0xF] = 0;
						}
						pc+=2;
						break;
					case 0x0005: 	// 0x8XY5
						/* subtracts VY from VX. VF is set to 0 if there is a borrow required, or 1 if there isn't */
						if(v[(opcode & 0x00F0) >> 4] > v[(opcode & 0x0F00) >> 8]) {	// if the number being subtracted is bigger (requiring carry) then ... 
							v[0xF] = 0;
						}
						else {
							v[0xF] = 1;
						}
						v[(opcode & 0x0F00) >> 8] -= v[(opcode & 0x00F0) >> 4];
						pc += 2;
						break;
					case 0x0006:	// 0x8XY6
						/* stores the least significant bit of VX in VF and shifts VX to the right by 1 */
						v[0xF] = v[(opcode & 0x0F00) >> 8] & 0x1;
						v[(opcode & 0x0F00) >> 8] >>= 1;
						pc+=2;
						break;
					case 0x0007:	// 0x8XY7
						/* subtracts VX from VX, VF is set to 0 if there is a borrow, 1 if not */
						if(v[(opcode & 0x00F0) >> 4] < v[(opcode & 0x0F00) >> 8]) {
							v[0xF] = 0;
						}
						else {
							v[0xF] = 1;
						}
						v[(opcode & 0x0F00) >> 8] = v[(opcode & 0x00F0) >> 4] - v[(opcode & 0x0F00) >> 8];
						pc+=2;
						break;
					case 0x000E:	// 0x8XYE
						/* stores the most significant bit of VX in VF and then shifts VX to the left by 1 */
						v[0xF] = v[(opcode & 0x0F00) >> 8] & 0xF;
						v[(opcode & 0x0F00) >> 8] <<= 1;
						pc+=2;
						break;
					}
					break;
			case 0x9000:	// 9XY0
				/* skips next instruction if VX doesn't equal VY */
				if(v[(opcode & 0x0F00) >> 8] != v[(opcode & 0x00F0) >> 4]) {
					pc+=4;
				}
				else {
					pc+=2;
				}
					break;
			case 0xA000:	// 0xANNN
				/* sets I to NNN */
				i = opcode & 0x0FFF;
				pc+=2;
				break;
			case 0xB000:	// 0xBNNN
				/* jump to the address NNN plus V0 */
				pc = v[0x0] + (opcode & 0x0FFF);
				break;
			case 0xC000:	// 0xCXNN
				/* sets VX to the result of an AND on a random number and NN */
				break;
			case 0xD000:	// 0xDXYN
				/* draws a sprite at [vx, vy] that has a width of 8 pixels and a height of N pixels. */
				//unsigned short x = v[(opcode & 0x0F00) >> 8];
				//unsigned short y = v[(opcode & 0x00F0) >> 4];
				//unsigned short h = opcode & 0x000F;

				break;
			}
	if(delay_timer > 0)
		--delay_timer;
	if(sound_timer > 0) {
		if(sound_timer == 0) {
			puts("BEEP");	// get an actual sound
			}
		--sound_timer;
	}
	return 0;
}



int main() {
	
	initialize();
	char *newline;
	char *choice = calloc(50, 1);

	puts("Please enter the file you would like to read from : ");
	if(fgets(choice, 50, stdin) == NULL) {
		perror("could not get choice ");
		abort();
	}
	
	if((newline = strchr(choice, '\n')) != NULL) {	// locate and remove trailing return
		*newline = '\0';
	}
	else { /* if there is no newline in the string because it was over the limit placed by fgets */
		puts("input too long");
		abort();
	}

	load_file(choice);	// read file specified into memory
	free(choice);

	for(int x = 512; x < MEM_LEN; x++) {
		fetch_execute(ram[x]);
	}
	return 0;
}
